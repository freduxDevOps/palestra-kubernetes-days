# Kube-hunter

O kube-hunter é uma ferramenta de código aberto projetada para identificar possíveis vulnerabilidades de segurança em clusters do Kubernetes. Ele executa uma série de testes automatizados e procura por possíveis pontos fracos em um ambiente Kubernetes. Siga as etapas abaixo para começar a usar o kube-hunter:

Passo 1: Instale o kube-hunter
Você pode instalar o kube-hunter executando o seguinte comando em seu terminal:

```
kubectl apply -f https://raw.githubusercontent.com/aquasecurity/kube-hunter/main/job.yaml
```

Passo 2: Execute a verificação de segurança
Após aplicar o manifesto, o kube-hunter verificará seu cluster. Verifique o nome do pod com o comando:

```
kubectl get pods -o name
```

Veja os logs:
```bash
kubectl logs kube-hunter-gqjbj
```

Passo 3: Analise os resultados
Após a conclusão da verificação de segurança, o kube-hunter exibirá os resultados no terminal. Exibirá algo como:

```bash
2023-07-06 21:48:44,924 INFO kube_hunter.modules.report.collector Started hunting
2023-07-06 21:48:44,924 INFO kube_hunter.modules.report.collector Discovering Open Kubernetes Services
2023-07-06 21:48:44,934 INFO kube_hunter.modules.report.collector Found vulnerability "CAP_NET_RAW Enabled" in Local to Pod (kube-hunter-w64xq)
2023-07-06 21:48:44,934 INFO kube_hunter.modules.report.collector Found vulnerability "Read access to pod's service account token" in Local to Pod (kube-hunter-w64xq)
2023-07-06 21:48:44,938 INFO kube_hunter.modules.report.collector Found vulnerability "Access to pod's secrets" in Local to Pod (kube-hunter-w64xq)
2023-07-06 21:49:00,078 INFO kube_hunter.modules.report.collector Found open service "Kubelet API" at 10.244.0.1:10250
2023-07-06 21:49:07,567 INFO kube_hunter.modules.report.collector Found open service "API Server" at 10.96.0.1:443
2023-07-06 21:49:07,625 INFO kube_hunter.modules.report.collector Found vulnerability "Access to API using service account token" in 10.96.0.1:443
2023-07-06 21:49:07,661 INFO kube_hunter.modules.report.collector Found vulnerability "K8s Version Disclosure" in 10.96.0.1:443

Nodes
+-------------+------------+
| TYPE        | LOCATION   |
+-------------+------------+
| Node/Master | 10.244.0.1 |
+-------------+------------+
| Node/Master | 10.96.0.1  |
+-------------+------------+

Detected Services
+-------------+------------------+----------------------+
| SERVICE     | LOCATION         | DESCRIPTION          |
+-------------+------------------+----------------------+
| Kubelet API | 10.244.0.1:10250 | The Kubelet is the   |
|             |                  | main component in    |
|             |                  | every Node, all pod  |
|             |                  | operations goes      |
|             |                  | through the kubelet  |
+-------------+------------------+----------------------+
| API Server  | 10.96.0.1:443    | The API server is in |
|             |                  | charge of all        |
|             |                  | operations on the    |
|             |                  | cluster.             |
+-------------+------------------+----------------------+

Vulnerabilities
For further information about a vulnerability, search its ID in: 
https://avd.aquasec.com/
+--------+----------------------+----------------------+----------------------+----------------------+----------------------+
| ID     | LOCATION             | MITRE CATEGORY       | VULNERABILITY        | DESCRIPTION          | EVIDENCE             |
+--------+----------------------+----------------------+----------------------+----------------------+----------------------+
| None   | Local to Pod (kube-  | Lateral Movement //  | CAP_NET_RAW Enabled  | CAP_NET_RAW is       |                      |
|        | hunter-w64xq)        | ARP poisoning and IP |                      | enabled by default   |                      |
|        |                      | spoofing             |                      | for pods.            |                      |
|        |                      |                      |                      |     If an attacker   |                      |
|        |                      |                      |                      | manages to           |                      |
|        |                      |                      |                      | compromise a pod,    |                      |
|        |                      |                      |                      |     they could       |                      |
|        |                      |                      |                      | potentially take     |                      |
|        |                      |                      |                      | advantage of this    |                      |
|        |                      |                      |                      | capability to        |                      |
|        |                      |                      |                      | perform network      |                      |
|        |                      |                      |                      |     attacks on other |                      |
|        |                      |                      |                      | pods running on the  |                      |
|        |                      |                      |                      | same node            |                      |
+--------+----------------------+----------------------+----------------------+----------------------+----------------------+
| KHV002 | 10.96.0.1:443        | Initial Access //    | K8s Version          | The kubernetes       | v1.26.3              |
|        |                      | Exposed sensitive    | Disclosure           | version could be     |                      |
|        |                      | interfaces           |                      | obtained from the    |                      |
|        |                      |                      |                      | /version endpoint    |                      |
+--------+----------------------+----------------------+----------------------+----------------------+----------------------+
| KHV005 | 10.96.0.1:443        | Discovery // Access  | Access to API using  | The API Server port  | b'{"kind":"APIVersio |
|        |                      | the K8S API Server   | service account      | is accessible.       | ns","versions":["v1" |
|        |                      |                      | token                |     Depending on     | ],"serverAddressByCl |
|        |                      |                      |                      | your RBAC settings   | ientCIDRs":[{"client |
|        |                      |                      |                      | this could expose    | CIDR":"0.0.0.0/0","s |
|        |                      |                      |                      | access to or control | ...                  |
|        |                      |                      |                      | of your cluster.     |                      |
+--------+----------------------+----------------------+----------------------+----------------------+----------------------+
| None   | Local to Pod (kube-  | Credential Access // | Access to pod's      | Accessing the pod's  | ['/var/run/secrets/k |
|        | hunter-w64xq)        | Access container     | secrets              | secrets within a     | ubernetes.io/service |
|        |                      | service account      |                      | compromised pod      | account/namespace',  |
|        |                      |                      |                      | might disclose       | '/var/run/secrets/ku |
|        |                      |                      |                      | valuable data to a   | bernetes.io/servicea |
|        |                      |                      |                      | potential attacker   | ...                  |
+--------+----------------------+----------------------+----------------------+----------------------+----------------------+
| KHV050 | Local to Pod (kube-  | Credential Access // | Read access to pod's | Accessing the pod    | eyJhbGciOiJSUzI1NiIs |
|        | hunter-w64xq)        | Access container     | service account      | service account      | ImtpZCI6IlpJVUJMeWto |
|        |                      | service account      | token                | token gives an       | WG5HWlpRbTNSOTlVeWNT |
|        |                      |                      |                      | attacker the option  | MFRjZDlHUUM5dWpqbU5K |
|        |                      |                      |                      | to use the server    | YS1kZjgifQ.eyJhdWQiO |
|        |                      |                      |                      | API                  | ...                  |
+--------+----------------------+----------------------+----------------------+----------------------+----------------------+
```

Ele fornecerá uma lista de possíveis vulnerabilidades encontradas em seu cluster do Kubernetes, juntamente com uma descrição detalhada de cada uma.

Os resultados serão categorizados em diferentes níveis de severidade, como "Baixa", "Média" e "Alta". É importante prestar atenção especial às vulnerabilidades de alta severidade, pois elas representam um risco maior à segurança do seu cluster.

Passo 4: Corrigir as vulnerabilidades
Com base nos resultados fornecidos pelo kube-hunter, você deve identificar as possíveis vulnerabilidades em seu cluster do Kubernetes. Siga as recomendações fornecidas pelo kube-hunter para corrigir essas vulnerabilidades.

Cada vulnerabilidade identificada será acompanhada por uma descrição do problema e sugestões sobre como mitigá-lo. Faça as alterações necessárias em seu cluster para remediar as vulnerabilidades encontradas.

Passo 5: Verificação periódica
Lembre-se de que a segurança é um processo contínuo. É recomendável executar regularmente o kube-hunter em seu cluster do Kubernetes para identificar possíveis novas vulnerabilidades ou alterações no ambiente que possam afetar a segurança.

Execute o kube-hunter em intervalos regulares e revise os resultados para garantir que o seu cluster esteja protegido contra as últimas ameaças de segurança.