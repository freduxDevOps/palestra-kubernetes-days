---
marp: true
theme: uncover
paginate: true
backgroundColor: #141414
color: #fff
colorSecondary: #10b5bf
backgroundImage: url('images/bg-slides.png')
style: |
    section{
      font-family: "Helvetica", monospace;
    }
    section::after {
      font-weight: bold;
      content: attr(data-marpit-pagination)'/'attr(data-marpit-pagination-total);
      font-size: 9pt;
      color: #10b5bf;
      text-shadow: 0px 0px 0 #fff;
    }    
---
<!-- _backgroundImage: url('images/main-background.png') -->
<style scoped>
  h2 {
    font-size: 40pt;
    list-style-type: circle;
    font-weight: 900;
    color: #fff
  }
  p {
    font-size: 20pt;
    font-weight: bold;
    list-style-type: circle;
    font-weight: 500;
    color: #10b5bf
  }
</style>

<!-- _paginate: false -->

# **Blindando o Kubernetes**
### Mitigando Riscos e Ameaças
#
#
Samuel Gonçalves

---

<!-- _paginate: false -->
> ## *O inventor, como a natureza de Linnaeus, não faz saltos; progride de manso, evolui.*

*Alberto Santos Dumont*

---
<style scoped>
  h4 {
    font-size: 35pt;
    list-style-type: circle;
    font-weight: 900;
    color: #10b5bf
  }
  p {
    font-size: 13pt;
  }
  {
   font-size: 28px;
  }
  img[alt~="center"] {
    display: block;
    margin: 0 auto;
  }

</style>
<!-- _backgroundImage: url('none') -->
<!-- _paginate: false -->

![bg right:40%](images/perfil.png)

#### 🇧🇷 Samuel Gonçalves

* Tech Lead na 4Linux
* Consultor de Tecnologia nas áreas **Segurança Ofensiva** e **DevSecOps**
* Mais de 10 anos de experiência 💻
* *"5k++"* alunos ensinados no 🌎
* Músico, Contista, YouTuber e Podcaster
* Apaixonado por Software Livre 💙
* Contato: [https://beacons.ai/sgoncalves](https://beacons.ai/sgoncalves)

---
<style scoped>
  p {
  font-size: 25pt;
  list-style-type: circle;
}
</style>
![bg right:40% 80%](images/qrcode-slides.jpg)
### Esses slides são OpenSource! 
Escaneie e acesse!

---
<style scoped>
  p {
  font-size: 25pt;
  list-style-type: circle;
}
</style>
![bg left:40% 80%](images/qrcode-contato-white.png)
### Vamos nos conectar?
Ou acesse: [beacons.ai/sgoncalves](https://beacons.ai/sgoncalves)

---
<style scoped>
  h2 {
    font-size: 50pt;
    list-style-type: circle;
    text-shadow: 3px 3px 8px #082a44;
    font-weight: 1000;
    color: #fff
  }
  p {
    font-size: 30pt;
  }
  {
   font-size: 35px;
  }
</style>

<!-- _paginate: false -->

## Dev**Sec**Ops

![w:900px](images/devsecops-banner.png)

---

<!-- _paginate: false -->
![bg w:1300px](images/4c.png)

---
<style scoped>
  h2 {
    font-size: 39pt;
    list-style-type: circle;
    font-weight: 1000;
    color: #10b5bf;
    text-align: left;
  }
  p {
    font-size: 30;
    text-align: left;
  }
  {
   font-size: 30px;
   text-align: left;
  }
</style>
## Segurança e Kubernetes

* Segurança é um dos principais desafios na execução do Kubernetes, de acordo com análise.
* Dificuldade decorre da presença de várias camadas móveis na stack nativa da nuvem.
* Engenheiros podem não dar ênfase à segurança desde o início.
* Algumas distribuições do Kubernetes podem não ser seguras por padrão, contrariando a suposição dos engenheiros.

---
<style scoped>
  h2 {
    font-size: 39pt;
    list-style-type: circle;
    font-weight: 1000;
    color: #10b5bf;
    text-align: left;
  }
  p {
    font-size: 30;
    text-align: left;
  }
  {
   font-size: 30px;
   text-align: left;
  }
</style>
## Previna, detecte e responda!

- Segurança da informação é um processo em constante evolução.
- É uma jornada contínua, não um destino final.
- Fases do processo de Segurança da Informação: prevenção, detecção e resposta.
- Engloba diversas estratégias e atividades ao longo dessas fases.

---
<style scoped>
  h2 {
    font-size: 39pt;
    list-style-type: circle;
    font-weight: 1000;
    color: #10b5bf;
    text-align: left;
  }
  p {
    font-size: 35;
    text-align: left;
  }
  {
   font-size: 35px;
   text-align: left;
  }
</style>

## Visão geral da segurança no Kubernetes
#
#
Precisamos nos preocupar em proteger dois grandes grupos de componentes do k8s:
* Proteger os componentes do cluster que são configuráveis
* Proteger os aplicativos executados no cluster

---
<style scoped>
  h2 {
    font-size: 39pt;
    list-style-type: circle;
    font-weight: 1000;
    color: #10b5bf;
    text-align: left;
  }
  p {
    font-size: 35;
    text-align: left;
  }
  {
   font-size: 35px;
   text-align: left;
  }
</style>

## Controle o acesso à API do Kubernetes
* Use Transport Layer Security (TLS) para todo o tráfego da API
* Configure a Autenticação da API
* Configure a Autorização da API

---
<style scoped>
  h2 {
    font-size: 39pt;
    list-style-type: circle;
    font-weight: 1000;
    color: #10b5bf;
    text-align: left;
  }
  p {
    font-size: 35;
    text-align: left;
  }
  {
   font-size: 35px;
   text-align: left;
  }
</style>

## Controle o acesso ao Kubelet 

* Expõe endpoints HTTPS para controle do nó e contêineres
* Acesso não autenticado à API dos Kubelets é permitido por padrão
* Clusters de produção devem habilitar autenticação e autorização do Kubelet

---
<style scoped>
  h2 {
    font-size: 39pt;
    list-style-type: circle;
    font-weight: 1000;
    color: #10b5bf;
    text-align: left;
  }
  p {
    font-size: 35;
    text-align: left;
  }
  {
   font-size: 30px;
   text-align: left;
  }
</style>

## Controle o uso de recursos

* Limite o uso de recursos em um cluster por meio de cotas e limits
* Controle com quais privilégios os contêineres são executados
* Impeça que contêineres carreguem módulos de kernel indesejados
* Restrinja o acesso à rede
* Restrinja o acesso à API de metadados da nuvem
* Controle o posicionamento dos pods 

--- 
<style scoped>
  h2 {
    font-size: 39pt;
    list-style-type: circle;
    font-weight: 1000;
    color: #10b5bf;
    text-align: left;
  }
  p {
    font-size: 35;
    text-align: left;
  }
  {
   font-size: 35px;
   text-align: left;
  }
</style>

## Proteja os componentes do cluster

* Restrinja o acesso ao etcd
* Ative o registro de auditoria
* Restrinja o acesso a recursos alfa ou beta 
* Alterne as credenciais de infraestrutura com frequência
* Revise as integrações de terceiros antes de ativá-las 
* Criptografar segredos em repouso 

---
<style scoped>
  h2 {
    font-size: 39pt;
    list-style-type: circle;
    font-weight: 1000;
    color: #10b5bf;
    text-align: left;
  }
  p {
    font-size: 35;
    text-align: left;
  }
  {
   font-size: 35px;
   text-align: left;
  }
</style>

## Checklist de Segurança k8s

A Cloud Native preparou um checklist completo de segurança para ser aplicado em clusters k8s. Você pode acessar pelo link: [kubernetes.io/docs/concepts/security/security-checklist/](https://kubernetes.io/docs/concepts/security/security-checklist/).

É muito importante garantir que todos os pontos possíveis estejam devidamente configurados.

---
<style scoped>
  h2 {
    font-size: 39pt;
    list-style-type: circle;
    font-weight: 1000;
    color: #10b5bf;
    text-align: left;
  }
  p {
    font-size: 35;
    text-align: left;
  }
  {
   font-size: 35px;
   text-align: left;
  }
</style>

## Ferramentas de verificação de segurança no k8s

Existem diversas ferramentas para verificação de segurança no k8s, iremos focar em 3: kube-bench, kube-hunter e kubesec.

---
<style scoped>
  h2 {
    font-size: 39pt;
    list-style-type: circle;
    font-weight: 1000;
    color: #10b5bf;
    text-align: left;
  }
  p {
    font-size: 35;
    text-align: left;
  }
  {
   font-size: 35px;
   text-align: left;
  }
</style>

## Kube-bench

- Trata-se de uma ferramenta para verificar segurança da implantação do Kubernetes.
- Executa verificações do CIS Kubernetes Benchmark.
- Testes configurados com arquivos YAML, facilitando atualizações conforme as especificações evoluem.

---
<style scoped>
  h2 {
    font-size: 39pt;
    list-style-type: circle;
    font-weight: 1000;
    color: #10b5bf;
    text-align: left;
  }
  p {
    font-size: 35;
    text-align: left;
  }
  {
   font-size: 35px;
   text-align: left;
  }
</style>

## Instalando `kube-bench`

Aplique o manifesto disponível no repositório do projeto, com o comando:
```groovy
kubectl apply -f https://raw.githubusercontent.com/aquasecurity/kube-bench/main/job.yaml
```

Ou, se você estiver na GCP como eu, execute:
```groovy
kubectl apply -f https://raw.githubusercontent.com/aquasecurity/kube-bench/main/job-gke.yaml
```

---
<style scoped>
  h2 {
    font-size: 30pt;
    list-style-type: circle;
    font-weight: 1000;
    color: #10b5bf;
    text-align: left;
  }
  p {
    font-size: 20;
    text-align: left;
  }
  {
   font-size: 30px;
   text-align: left;
  }
</style>

## Avaliando resultados `kube-bench`

Basta verificar os logs do pod com um comando semelhante a:
```groovy
# kubectl logs kube-bench-rf2g8
[...]
======================== Summary managedservices ========================
0 checks PASS
0 checks FAIL
37 checks WARN
0 checks INFO
============================= Summary total =============================
12 checks PASS
2 checks FAIL
63 checks WARN
0 checks INFO
```

---
<style scoped>
  h2 {
    font-size: 39pt;
    list-style-type: circle;
    font-weight: 1000;
    color: #10b5bf;
    text-align: left;
  }
  p {
    font-size: 35;
    text-align: left;
  }
  {
   font-size: 35px;
   text-align: left;
  }
</style>

## kube-hunter

- O kube-hunter busca por vulnerabilidades de segurança em clusters Kubernetes.
- A ferramenta foi desenvolvida para aumentar a conscientização e visibilidade de problemas de segurança em ambientes Kubernetes.
- NÃO execute o kube-hunter em um cluster Kubernetes que não seja de sua propriedade!

---
<style scoped>
  h2 {
    font-size: 39pt;
    list-style-type: circle;
    font-weight: 1000;
    color: #10b5bf;
    text-align: left;
  }
  p {
    font-size: 35;
    text-align: left;
  }
  {
   font-size: 33px;
   text-align: left;
  }
</style>
## Executando o `kube-hunter`

- O kube-hunter está disponível como um contêiner (aquasec/kube-hunter).
- É possível acessar `kube-hunter.aquasec.com`, visualizar e compartilhar os resultados de seu escaneamento online.
- Você também pode executar o código Python por conta própria.

---
<style scoped>
  h2 {
    font-size: 39pt;
    list-style-type: circle;
    font-weight: 1000;
    color: #10b5bf;
    text-align: left;
  }
  p {
    font-size: 35;
    text-align: left;
  }
  {
   font-size: 35px;
   text-align: left;
  }
</style>

## Instalando `kube-hunter`

Aplique o manifesto disponível no repositório do projeto, com o comando:
```groovy
kubectl apply -f https://raw.githubusercontent.com/aquasecurity/kube-hunter/main/job.yaml
```

---
<style scoped>
  h2 {
    font-size: 30pt;
    list-style-type: circle;
    font-weight: 1000;
    color: #10b5bf;
    text-align: left;
  }
  p {
    font-size: 20;
    text-align: left;
  }
  {
   font-size: 39px;
   text-align: left;
  }
</style>

## Avaliando resultados `kube-hunter`

```groovy
# kubectl logs kube-hunter-w64xq
Nodes
+-------------+------------+
| TYPE        | LOCATION   |
+-------------+------------+
| Node/Master | 10.244.0.1 |
+-------------+------------+
| Node/Master | 10.96.0.1  |
+-------------+------------+
```

---
<style scoped>
  h2 {
    font-size: 30pt;
    list-style-type: circle;
    font-weight: 1000;
    color: #10b5bf;
    text-align: left;
  }
  p {
    font-size: 20;
    text-align: left;
  }
  {
   font-size: 28px;
   text-align: left;
  }
</style>

## Avaliando resultados `kube-hunter`

```groovy
# kubectl logs kube-hunter-w64xq
Detected Services
+-------------+------------------+----------------------+
| SERVICE     | LOCATION         | DESCRIPTION          |
+-------------+------------------+----------------------+
| Kubelet API | 10.244.0.1:10250 | The Kubelet is the   |
|             |                  | main component in    |
|             |                  | every Node, all pod  |
|             |                  | operations goes      |
|             |                  | through the kubelet  |
+-------------+------------------+----------------------+
| API Server  | 10.96.0.1:443    | The API server is in |
|             |                  | charge of all        |
|             |                  | operations on the    |
|             |                  | cluster.             |
+-------------+------------------+----------------------+
```

---
<style scoped>
  h2 {
    font-size: 30pt;
    list-style-type: circle;
    font-weight: 1000;
    color: #10b5bf;
    text-align: left;
  }
  p {
    font-size: 20;
    text-align: left;
  }
  {
   font-size: 35px;
   text-align: left;
  }
</style>

## Avaliando resultados `kube-hunter` no terminal

---
<style scoped>
  h2 {
    font-size: 35pt;
    list-style-type: circle;
    font-weight: 1000;
    color: #10b5bf;
    text-align: left;
  }
  p {
    font-size: 20;
    text-align: left;
  }
  {
   font-size: 30px;
   text-align: left;
  }
</style>

## Kubesec

- Ferramenta de análise e escaneamento de segurança de código aberto para o Kubernetes.
- Funciona ao receber um único arquivo de manifesto do Kubernetes e fornece uma pontuação de gravidade para cada vulnerabilidade encontrada.
- Usando o Kubesec, você pode revisar os manifestos dos recursos e impor políticas verificando as regras específicas durante as fases iniciais do ciclo de desenvolvimento.

---
<style scoped>
  h2 {
    font-size: 35pt;
    list-style-type: circle;
    font-weight: 1000;
    color: #10b5bf;
    text-align: left;
  }
  p {
    font-size: 20;
    text-align: left;
  }
  {
   font-size: 30px;
   text-align: left;
  }
</style>

## Instalação do `kubesec`
O Kubesec pode ser instalado como um pacote binário, imagem de contêiner, controlador de admissão ou até mesmo como um plugin do kubectl. Por exemplo:

```groovy
# wget https://github.com/controlplaneio/kubesec/releases/download/v2.11.4/kubesec_linux_amd64.tar.gz
# tar -xvf kubesec_linux_amd64.tar.gz
```

---
<style scoped>
  h2 {
    font-size: 35pt;
    list-style-type: circle;
    font-weight: 1000;
    color: #10b5bf;
    text-align: left;
  }
  p {
    font-size: 20;
    text-align: left;
  }
  {
   font-size: 35px;
   text-align: left;
  }
</style>

## Analisando manifestos

Você pode analisar os manifestos com o comando `kubesec scan`, e teremos um resultado semelhante a:

```groovy
"object": "Pod/security-context-demo.default",
"valid": true,
"fileName": "pod-insecure.yml",
"message": "Failed with a score of -30 points",
"score": -30,
```

---
<!-- _paginate: false -->
<style scoped>
  h2 {
    font-size: 35pt;
    list-style-type: circle;
    font-weight: 1000;
    color: #10b5bf;
    text-align: left;
  }
  h3 {
    font-size: 25pt;
    list-style-type: circle;
    color: #fff;
    text-align: center;
  }
  p {
    font-size: 20;
    text-align: left;
  }
  {
   font-size: 30px;
   text-align: left;
  }
</style>
![bg right:40%](images/curso.png)

## Segurança é JORNADA!
### Curso "Segurança em cluster Kubernetes - CKS" da 4Linux!

Converse **AGORA** com nossos contultores e garanta um super desconto especial!

---
<!-- _backgroundImage: url('images/bg-duvidas.png') -->
<style scoped>
  h2 {
    font-size: 55pt;
    list-style-type: circle;
    font-weight: 1000;
    color: #10b5bf;
    text-align: center;
  }
  p {
    font-size: 20;
    text-align: left;
  }
  {
   font-size: 35px;
   text-align: left;
  }
</style>

## Dúvidas?


---
<style scoped>
  h2 {
    font-size: 35pt;
    list-style-type: circle;
    font-weight: 1000;
    color: #10b5bf;
    text-align: left;
  }
  p {
    font-size: 30;
    text-align: left;
  }
  {
   font-size: 25px;
   text-align: left;
  }
</style>

## Referências

https://kubernetes.io/docs/concepts/security/overview/
https://kubernetes.io/docs/tasks/administer-cluster/securing-a-cluster/
https://kubernetes.io/docs/concepts/security/security-checklist/
https://devopslearning.medium.com/kubesec-e02a8a00d2be
https://github.com/falcosecurity/falco
https://falco.org/docs/getting-started/try-falco/try-falco-on-kubernetes/
https://thenewstack.io/top-challenges-kubernetes-users-face-deployment/
https://academia.jac.bsb.br/ver/artigo/seguranca-basica-em-kubernetes/
https://blog.4linux.com.br/kubernetes-avaliando-a-seguranca-do-cluster-part-1/
https://github.com/aquasecurity/kube-hunter