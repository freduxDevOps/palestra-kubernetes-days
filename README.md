# Blindando o Kubernetes: Mitigando Riscos e Ameaças

Repositório para armazenar os Slides da palestra **Blindando o Kubernetes: Mitigando Riscos e Ameaças** ministrado por:

- [Samuel Gonçalves](https://beacons.ai/sgoncalves)

No Kubernetes Days da [4Linux](https://4linux.com.br/) em 2023, evento realizado no YouTube.

## whoami

### 🇧🇷 Samuel Gonçalves

* Tech Lead na 4Linux
* Consultor de Tecnologia nas áreas **Segurança Ofensiva** e **DevSecOps**
* Mais de 10 anos de experiência 💻
* *"5k++"* alunos ensinados no 🌎
* Músico, Contista, YouTuber e Podcaster
* Apaixonado por Software Livre 💙

> Contato: [https://beacons.ai/sgoncalves](https://beacons.ai/sgoncalves)

## Objetivos da Palestra

1. Compreender os princípios do DevSecOps
2. Compreender as ameaças e riscos associados ao ambiente do cluster Kubernetes.
3. Explorar as melhores práticas de segurança DevSecOps para proteger o cluster Kubernetes.
4. Aprender a utilizar a ferramenta Kubesec para avaliação de riscos no cluster Kubernetes.
5. Aprender a utilizar a ferramenta kube-hunter para avaliação de riscos no cluster Kubernetes.
6. Aprender a utilizar a ferramenta kube-bench para avaliação de riscos no cluster Kubernetes.
7. Obter insights e estratégias adicionais para garantir a segurança e proteção do ambiente Kubernetes.
8. Capacitar os participantes a implementar medidas de segurança efetivas em seus clusters Kubernetes.